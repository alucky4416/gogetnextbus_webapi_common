
package main

import (
//  "net/http"
  "io"
  "os"
  "path/filepath"
  "html/template"
  "net/http"
  "github.com/labstack/echo"
  "./api"
//  "fmt"
)


type Template struct {
  templates *template.Template
}

type ServiceInfo struct {
  Title string
}

var serviceInfo = ServiceInfo {
  "",
}
const DISTINATION_NAME = "Distination Name"

const FILENAME_TIMETABLE = "timetable.csv"

const PATH_TEMPLATE_VIEWS = "views/*.html"
const PATH_STATIC = "static"

// ---------------------------------------------------------------------
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
  return t.templates.ExecuteTemplate(w, name, data)
}

func main() {

	rootpath := filepath.Dir(os.Args[0])
//  fmt.Printf("rootpath = %s\n", rootpath)

	timetablepath := filepath.Join(rootpath, FILENAME_TIMETABLE)
	templatepath_view := filepath.ToSlash(filepath.Join(rootpath, PATH_TEMPLATE_VIEWS))
	staticpath := filepath.ToSlash(filepath.Join(rootpath, PATH_STATIC))

//  fmt.Printf("templatepath_view = %s\n",templatepath_view)

//  api.InitTimeTable("C:/gogetnextbus/timetable.csv", "")
  api.InitTimeTable(timetablepath)

  t := &Template{
//    templates: template.Must(template.ParseGlob("C:/gogetnextbus/views/*.html")),
    templates: template.Must(template.ParseGlob(templatepath_view)),
  }

  e := echo.New()

  e.Static("/", staticpath)

  e.Renderer = t

//  e.GET("/v1/nextbustime_csv", api.GetNextBus_csv)
//  e.GET("/v1/nextbustime_json", api.GetNextBus_json)

  e.GET("/nextbustime", func(c echo.Context) error {
    data := struct {
      ServiceInfo
			Distination string
      Next_bus_time string
      Nextnext_bus_time string
      Nowtime string
    } {
      ServiceInfo: serviceInfo,
			Distination: DISTINATION_NAME,
      Next_bus_time: "--:--",
      Nextnext_bus_time: "--:--",
      Nowtime: "--:--",
    }
    next, nextnext := api.SearchNextBus()
    data.Next_bus_time = next
    if next == "--:--" {
      data.Nextnext_bus_time = "Outside business hours"
    } else {
      if nextnext == "--:--" {
        data.Nextnext_bus_time = "Final"
      } else {
        data.Nextnext_bus_time = nextnext
      }
    }
    data.Nowtime = api.GetNowTimeString()

    return c.Render(http.StatusOK, "nextbustime", data)
  })

//  e.Logger.Fatal(e.Start(":8080"))
  e.Logger.Fatal(e.Start(":3001"))

}
