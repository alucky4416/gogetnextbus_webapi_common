package api

import (
//  "syscall"
//  "io/ioutil"
//  "encoding/json"
  "fmt"
  "time"
  "net/http"
  "github.com/labstack/echo"

  "os"
  "encoding/csv"
  "strconv"
)

const location = "Asia/Tokyo"

//const timeformat = "2006/01/02 15:04:05"
const timeformat = "15:04:05"

var timetable []int

// timetable load from csv filem and set hhmm time data
func load_timetable_from_csv(filename string, timetable *[]int) {

  file, err := os.Open(filename)
  if err != nil {
    //panic(err)
    return
  }
  defer file.Close()

  reader := csv.NewReader(file)
  var line []string

  for {
    line, err = reader.Read()
    if err != nil {
      break
    }
    fmt.Println(line)
    hour, err1 := strconv.Atoi(line[0])
    if err1 != nil { // error atoi()
      break
    }
    for i := 1; i < len(line); i++ {
      min, err2 := strconv.Atoi(line[i])
      if err2 == nil { // when success atoi()
        *timetable = append(*timetable, (hour*100 + min))
      }
    }
  }

}

func InitTimeTable(timetable_csv string) {

  // set timezone
	loc, err := time.LoadLocation(location)
	if err != nil {
		loc = time.FixedZone(location, 9*60*60)
	}
	time.Local = loc

	// load timetable from csv file
  timetable = make([]int, 0, 200)
  load_timetable_from_csv(timetable_csv, &timetable)

}

func GetNowTimeString() string {
  t := time.Now()

//  hh := t.Hour()
//  mm := t.Minute()
  hh, mm, _ := t.Clock()
  s := fmt.Sprintf("%02d:%02d", hh, mm)

  return s
}

func search_next_bustime(timetable *[]int) (string, string) {

  next := "--:--" // "00:00" is service ended
  nextnext := "--:--" // "00:00" is service ended

  // search next time from timetable by now
  //now := 820 // 08:20
  t := time.Now()

  // Satruday and Sunday is outside business hours
  weekday_n := t.Weekday()
  if time.Sunday == weekday_n || time.Saturday == weekday_n {
    return next, nextnext
  }

  nowtime := t.Add(1 * time.Minute) // after 1 min
  now := nowtime.Hour() * 100 + nowtime.Minute()

  i := 0
  for i = 0; i < len(*timetable); i++ {
    if (*timetable)[i] > now {
      break
    }
  }
  if i >= len(*timetable) {
    // fmt.Printf("not found, service ended \n")
  } else {
    hh := (*timetable)[i] / 100
    mm := (*timetable)[i] % 100
    next = fmt.Sprintf("%02d:%02d\n", hh, mm)
    if i+1 >= len(*timetable) {
      ;
    } else {
      hh2 := (*timetable)[i+1] / 100
      mm2 := (*timetable)[i+1] % 100
      nextnext = fmt.Sprintf("%02d:%02d\n", hh2, mm2)
    }
  }
  return next, nextnext
}

func SearchNextBus() (string, string) {

  next := "--:--" // "00:00" is service ended
  nextnext := "--:--" // "00:00" is service ended
  next, nextnext = search_next_bustime(&timetable)

  return next, nextnext
}


func GetNextBus_csv(c echo.Context) error {
  t := time.Now()
  fmt.Printf("%s: receive from %s: GetNextBus_csv.\n", t.Format(timeformat), c.RealIP())

  next, nextnext := SearchNextBus()
  result := fmt.Sprintf("%s, %s", next, nextnext)
  return c.String(http.StatusOK, result)
}
/*
func GetNextBus_json(c echo.Context) error {
  t := time.Now()
  fmt.Printf("%s: receive from %s: GetNextBus_json.\n", t.Format(timeformat), c.RealIP())

  next, nextnext := SearchNextBus()
  result := fmt.Sprintf("%s, %s", next, nextnext)
  return c.JSON(http.StatusOK, result)
}
*/
